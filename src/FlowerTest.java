import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Created by nickseagull on 13/6/17.
 */
public class FlowerTest {
    @Test
    public void shouldBeAbleToRegisterObservers() {
        Flower flower = new Flower();
        Bee bee = new Bee();

        flower.register(bee);

        assertEquals(flower.getFlowerObservers().get(0), bee);
    }

    @Test
    public void shouldBeAbleToUnregisterObservers() {
        Flower flower = new Flower();
        Bee bee = new Bee();

        flower.register(bee);
        flower.unregister(bee);

        assertTrue(flower.getFlowerObservers().isEmpty());
    }

    @Test
    public void shouldBeAbleToOpenItsPetals() {
        Flower flower = new Flower();
        Bee bee = new Bee();

        flower.register(bee);
        flower.open();

        assertTrue(bee.isHavingBreakfast());
    }

    @Test
    public void shouldBeAbleToCloseItsPetals() {
        Flower flower = new Flower();
        Bee bee = new Bee();
        Hummingbird hummingbird = new Hummingbird();

        flower.register(bee);
        flower.register(hummingbird);
        flower.open();
        flower.close();

        assertFalse(bee.isHavingBreakfast());
        assertFalse(hummingbird.isHavingBreakfast());
    }

    @Test
    public void shouldOnlyMakeObserversReactOnce() {
        Flower flower = new Flower();
        Bee bee = new Bee();
        Hummingbird hummingbird = new Hummingbird();

        flower.register(bee);
        flower.register(hummingbird);
        flower.open();

        assertTrue(bee.isHavingBreakfast());
        assertTrue(hummingbird.isHavingBreakfast());

        flower.open();

        assertTrue(bee.isHavingBreakfast());
        assertTrue(hummingbird.isHavingBreakfast());

        flower.close();

        assertFalse(bee.isHavingBreakfast());
        assertFalse(hummingbird.isHavingBreakfast());

        flower.close();

        assertFalse(bee.isHavingBreakfast());
        assertFalse(hummingbird.isHavingBreakfast());
    }

}
