import java.util.ArrayList;

/**
 * Created by nickseagull on 13/6/17.
 */
public class Flower {
    private boolean open;
    private ArrayList<FlowerObserver> flowerObservers;

    public Flower (){
        this.flowerObservers = new ArrayList<>();
        this.open = false;
    }

    public void register(FlowerObserver flowerObserver) {
        this.flowerObservers.add(flowerObserver);
    }

    public void unregister(FlowerObserver flowerObserver) {
        this.flowerObservers.remove(flowerObserver);
    }

    public ArrayList<FlowerObserver> getFlowerObservers() {
        return flowerObservers;
    }

    public void open(){
        if (open) return;
        this.flowerObservers
                .stream()
                .forEach(flowerObserver -> flowerObserver.notifyOpen());
        this.open = true;
    }

    public void close(){
        if (!open) return;
        this.flowerObservers
                .stream()
                .forEach(flowerObserver -> flowerObserver.notifyClose());
        this.open = false;
    }
}
