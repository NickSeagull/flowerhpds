/**
 * Created by nickseagull on 13/6/17.
 */
public abstract class FlowerObserver {
    protected boolean isHavingBreakfast;

    public FlowerObserver(){
        this.isHavingBreakfast = false;
    }

    public void notifyOpen(){
        this.isHavingBreakfast = true;
    }

    public void notifyClose(){
        this.isHavingBreakfast = false;
    }

    public boolean isHavingBreakfast() {
        return this.isHavingBreakfast;
    }
}
